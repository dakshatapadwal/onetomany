package com.mapping.OneToMany;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mobile3")
public class Mobile {
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name="mob1")
	private int mob1;
	@Column(name="mob2")
	private int mob2;
	@Column(name="mob3")
	private int mob3;
	public int getMob1() {
		return mob1;
	}
	public void setMob1(int mob1) {
		this.mob1 = mob1;
	}
	public int getMob2() {
		return mob2;
	}
	public void setMob2(int mob2) {
		this.mob2 = mob2;
	}
	public int getMob3() {
		return mob3;
	}
	public void setMob3(int mob3) {
		this.mob3 = mob3;
	}

}
