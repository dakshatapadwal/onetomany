package com.mapping.OneToMany;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Main {
	public static void main(String args[])
	{
		Configuration conf = new Configuration();
		Session session = conf.configure("com/mapping/OneToMany/hibernate.cfg.xml").buildSessionFactory().openSession();
		Transaction tr = session.beginTransaction();
		
		Mobile m = new Mobile();
		//m.setId(999);
		m.setMob1(12345);
		m.setMob2(22756);
		m.setMob3(5555);
		
		Mobile m2 = new Mobile();
		//m.setId(666);
		m.setMob1(333);
		m.setMob2(555);
		m.setMob3(444);

		Mobile m3 = new Mobile();
		//m.setId(909);
		m.setMob1(777);
		m.setMob2(666);
		m.setMob3(333);
		 
		Set<Mobile> set = new  HashSet<Mobile>();
		set.add(m);
		Set<Mobile> set1 = new  HashSet<Mobile>();
		set1.add(m2);
		Set<Mobile> set2 = new  HashSet<Mobile>();
		set2.add(m3);
		
		
		
		Employee e1= new Employee();
		//e1.setId(1);
		e1.setF_name("a");
		e1.setL_Name("b");
		e1.setSet(set);
		e1.setSet(set);
		
		Employee e2= new Employee();
		//e2.setId(2);
		e2.setF_name("x");
		e2.setL_Name("y");
		e2.setSet(set1);
		
		Employee e3= new Employee();
		//e3.setId(3);
		e3.setF_name("u");
		e3.setL_Name("v");
		e3.setSet(set2);
		session.save(e1);
		session.save(e2);
		session.save(e3);
		
		Set<Employee> s = new HashSet<Employee>();
		s.add(e1);
		s.add(e2);
		s.add(e3);
		
		Company c = new Company();
		//c.setId(8907);
		c.setCom_Name("xyzww");
		c.setS(s);
		
		session.save(c);
		tr.commit();
		session.close();
		
		
	}

}
