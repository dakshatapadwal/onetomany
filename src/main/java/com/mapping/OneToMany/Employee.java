package com.mapping.OneToMany;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="employee3")
public class Employee {
	public Set<Mobile> getSet() {
		return set;
	}
	public void setSet(Set<Mobile> set) {
		this.set = set;
	}
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name="fname")
	private String f_name;
	@Column(name="lname")
	private String l_Name;
	@OneToMany(targetEntity=Mobile.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "cid1", referencedColumnName="id")
	private Set<Mobile>  set;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
	public String getL_Name() {
		return l_Name;
	}
	public void setL_Name(String l_Name) {
		this.l_Name = l_Name;
	}

}
