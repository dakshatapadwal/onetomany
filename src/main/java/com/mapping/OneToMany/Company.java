package com.mapping.OneToMany;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="company3")
public class Company {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name="compname")
	private String com_Name;
	
	@OneToMany(targetEntity=Employee.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "cid", referencedColumnName="id")
	private Set<Employee> s;
	
	
	public Set<Employee> getS() {
		return s;
	}
	public void setS(Set<Employee> s) {
		this.s = s;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCom_Name() {
		return com_Name;
	}
	public void setCom_Name(String com_Name) {
		this.com_Name = com_Name;
	}

}
